var builder = require('botbuilder');
var restify = require('restify');

var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
	console.log('%s listeninig to $s', server.name, server.url);
});



var inMemoryStorage = new builder.MemoryBotStorage();

var connector = new builder.ChatConnector({
	appID: '',
	appPassword: '',
});



var bot = new builder.UniversalBot(connector).set('storage', inMemoryStorage);
server.post('/api/messages', connector.listen());

bot.dialog('/',[
	function(session){
		builder.Prompts.text(session, ' Hi there! What is your name?')
	},
	(session, args) => {
		session.dialogData.name = args.response;
		builder.Prompts.text(session, `hello ${session.dialogData.name} , What is your profession?` );
	},

	(session, args) => {
		session.dialogData.profession = args.response;
		builder.Prompts.number(session, `Thank You ${session.dialogData.name}, what is your age?`);
	},

	(session, args) => {
		session.dialogData.age = args.response;
		builder.Prompts.time(session, 'What is the time right now in your country?');
	},

	(session, args) => {
		session.dialogData.time = builder.EntityRecognizer.resolveTime([args.response]);
		builder.Prompts.confirm(session, `So ${session.dialogData.name}, I have all your details, do you wish you see them?`);
	},

	(session, args) => {
		if (args.response) {
			session.endDialog(` Your Given Information are as follows:
				<br/> Name : ${session.dialogData.name}
				<br/> Profession : ${session.dialogData.profession}
				<br/> Age : ${session.dialogData.age}
				<br/> Time when entered the Information : ${session.dialogData.time}`);

		} else {
			session.endDialog("Thank You for your time, Bye!")
		}
	}
	

]);